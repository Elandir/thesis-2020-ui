Audio files transcripts
# Welcome:  
Welcome to Bolt!

# After loading: 
You can choose the route on the right panel by clicking the green arrow button. When you are done, press start. During the ride, you can choose your mood using the mood bar. This setting controls the current speed and distance from other vehicles. There is also a quick toggle at the bottom right corner for this.
For navigation, please use the button at the upper right corner or buttons at the bottom left corner. We hope you have a pleasant trip!

# Close destination:
We are nearing our destination. Please choose a place for stopping from the list below or let the vehicle decide. The stopping process will take usually 30 seconds to a minute, depending on the car position and traffic.

# Stopped:
The vehicle has stopped, you can now safely get off. Thank you for using our services and we hope to see you soon again!

# Actual stop:
You have chosen to stop the ride prematurely. Please choose the spot to get off or let the car decide. The stopping process will take usually 30 seconds to a minute, depending on the car position and traffic.

