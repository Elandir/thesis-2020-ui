# README #

Steps to get this bad boy up and running

1. make sure you have node js installed (version 10 +)
2. run "npm install"
3. run "npm run build" - this will build files in dist folder and starts watching for file changes
4. to make changes in the HTML, edit the the file located in src folder, not in dist folder.
5. this project does not have live server (yet), so in orer to actually run it, you have to open index.html located in dist folder. You can open it with chrome (doesnt listen to changes) or use the live server built into some of the code editors e.g. VS Code (extension)
6. If you should get an error about map loading it means that i have disabled the API and you shou should activate your own.

To serve it to other devices in the same network you must have localhost up and running (apache, i just installed xampp) and files should be located in your localhost serving folder. To access this project simply enter serving machines ip address (cmd -> ipconfig) and the directory to the dist folder e.g. 192.168.1.11/thesis-2020-ui/dist.

Have fun, for whatever reason you need this.