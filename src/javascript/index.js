// imports
import '../sass/main.scss';
import * as $ from 'jquery';
import bootstrap from 'bootstrap';
import popper from 'popper.js';
import '../../node_modules/jquery-hammerjs/jquery.hammer.js';
import { pureknob } from '../vendors/pureknob.js';

//global variables
let marker;
let startPos = [58.373661, 26.751070];
let delay = 100;
let coordsRoute1 = [];
let coordsRoute2 = [];
let coordsRoute3 = [];
let currentCoords = coordsRoute1;
let fadeSpeed = 500;
let currentSpeedContainer = $('.currentSpeed');
let etaContainer = $('.eta');
let endLocationContainer = $('#endLocation');
let startLocationContainer = $('#startLocation');
let map;
var line;
var pointDistances;
var directionsDisplay;
let speed = 0;
let currentDistanceContaienr = $('.currentDistance');
let drivingStyleButton = $('#driving-style-button');
let drivingStyleSlider = $('.driving-style');
let range = $('.range');
let started = false;
let routeTr = $('td.adp-listsel>span>span:nth-child(4)>span');
let invertMood = false;
let settings = {
  exiting: {
    speed: 80,
    distance: 0.5,
    text: invertMood ? 'Calm' : 'Alert'
  },
  fun: {
    speed: 70,
    distance: 1,
    text: invertMood ? 'Relaxed' : 'Exited'
  },
  neutral: {
    speed: 60,
    distance: 2,
    text: 'Move'
  },
  safe: {
    speed: 40,
    distance: 3,
    text: invertMood ? 'Exited' : 'Relaxed'
  },
  safest: {
    speed: 35,
    distance: 4,
    text: invertMood ? 'Alert' : 'Calm'
  }
};

$(document).ready(() => {
  // play welcome message 
  $('#welcomeAudio').get(0).play();

  // hide all
  hideAll();

  // init map
  initMap();

  // show modal for easier testing
  // $('#stopping-modal-close').modal('show');

  // aircon
  initAircon();

  // stop all audio
  $('.stopAudio').click(() => {
    let sounds = $('audio');
    for (let i = 0; i < sounds.length; i++) sounds[i].pause();
  });

  // welcome button init (appears after the map has been (hopefully) loaded)
  setTimeout(() => {
    $('.welcome__btn').removeClass('d-none');
    $('#pleaseWait').addClass('d-none');
  }, 5000);

  // quick guide button to also start the audio
  $('.quickGuide').click(() => {
    setTimeout(() => {
      $('#introAudio').get(0).play();
    }, 2000);
  });

  // hide tooltip
  $('.main-page__tooltip').click((event) => {
    $(event.currentTarget).addClass('d-none');
  });

  // sidebar button
  $('.main-page__side-bar-button').click((event) => {
    $(event.currentTarget).toggleClass('main-page__side-bar-button--is-open');
    $('#right-panel').toggleClass('main-page__side-bar--is-open');
    $('.main-page__tooltip').addClass('d-none');
  });

  // choose stopping location
  $('.modal__location-img').click((event) => {
    $('.modal__location').removeClass('modal__location--active');
    $('.modal__stopping-btn').removeClass('modal__stopping-btn--active');
    $('.modal__overlay').addClass('d-none');
    $(event.currentTarget).parent().addClass('modal__location--active');
    $(event.currentTarget).next().removeClass('d-none');
  });

  $('.modal__stopping-btn').click((event) => {
    $('.modal__location').removeClass('modal__location--active');
    $('.modal__overlay').addClass('d-none');
    $(event.currentTarget).addClass('modal__stopping-btn--active');
  });

  // range functionality
  moodRangeSlider(event);

  // hide welcome screen
  $('.welcome__btn').on('click', () => {
    $('.welcome').fadeOut(fadeSpeed, () => {
      $('.main-page, .header, .navigation, .driving-style, .main-page__side-bar-button').fadeIn(500);
      $('.navbar-collapse').removeAttr('style');
      // range functionality
      moodRangeSlider(event);
    });
    setTimeout(() => {
      $('#quick-guide-modal').modal('show');
      //$('#introAudio').get(0).play();
    }, 2000);
  });

  // driving style button for desktop
  drivingStyleButton.on('click', () => toggleDrivingStyle(drivingStyleButton, drivingStyleSlider));

  // navigation
  changePage('.mainPage', '.main-page', fadeSpeed);
  changePage('.carPage', '.car-page', fadeSpeed);
  changePage('.contactPage', '.contact-page', fadeSpeed);
  changePage('.entPage', '.ent-page', fadeSpeed);
  changePage('.settingsPage', '.settings-page', fadeSpeed);
  changePage('.infoPage', '.info-page', fadeSpeed);

  // burger button
  $('#nav-icon2').click((event) => {
    $(event.currentTarget).toggleClass('open');
  });

  // start/stop button
  $('#start').click((event) => {
    $('.main-page__tooltip').addClass('d-none');

    let time = parseInt($('td.adp-listsel>span>span:nth-child(4)>span').text().replace(/[^0-9]/g, '')) * 60000;
    $('.adp-list').css('display', 'none');

    $(event.currentTarget).addClass('d-none');
    $('#stop').removeClass('d-none');
    CountDown.Start(time);
    range.removeAttr('disabled');
    drivingStyleButton.removeAttr('disabled');
    $('.range').css('opacity', '1');
    started = true;

    // speed
    let startSpeed = setInterval(() => {
      if (speed <= 59) {
        speed += 1;
        currentSpeedContainer.text(speed + ' ');
      }
      else {
        clearInterval(startSpeed);
      }
    }, 100);

    // resets and draws new map
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: { lat: 58.378397, lng: 26.734660 },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var startMarker = new google.maps.Marker({
      position: { lat: 58.373661, lng: 26.751071 },
      map: map,
      label: {
        color: '#fff',
        text: 'A'
      }
    });

    var destinationMarker = new google.maps.Marker({
      position: { lat: 58.382020, lng: 26.721140 },
      map: map,
      label: {
        color: '#fff',
        text: 'B'
      }
    });

    let tds = $('.adp-fullwidth tr td');
    let index = 0;
    tds.each((i, element) => {
      if ($(element).hasClass('adp-listsel')) {
        index = i;
      }
    });

    if (index == 1) { currentCoords = coordsRoute1 }
    else if (index == 2) { currentCoords = coordsRoute2 }
    else if (index == 3) { currentCoords = coordsRoute3 }
    else { currentCoords = coordsRoute1 }

    var lineCoordinates = [];
    currentCoords.forEach((element) => {
      lineCoordinates.push(new google.maps.LatLng(element[0], element[1]));
    });
    var sphericalLib = google.maps.geometry.spherical;
    pointDistances = [0];
    for (var i = 1; i < lineCoordinates.length; i++) {
      var partialDistance = sphericalLib.computeDistanceBetween(
        lineCoordinates[i],
        lineCoordinates[i - 1]);
      pointDistances[i] = pointDistances[i - 1] + partialDistance;
      //console.log('pointDistances[' + i + ']: ' + pointDistances[i]);
    }
    // convert to percentage offset
    var wholeDist = pointDistances[pointDistances.length - 1];
    for (var i = 0; i < lineCoordinates.length; i++) {
      pointDistances[i] = 100 * pointDistances[i] / wholeDist;
      //console.log('pointDistances[' + i + ']: ' + pointDistances[i]);
    }

    // define polyline
    var lineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 8,
      strokeColor: '#2EB875'
    };

    line = new google.maps.Polyline({
      path: lineCoordinates,
      strokeColor: 'rgb(26, 115, 232)',
      strokeOpacity: 1.0,
      strokeWeight: 4,
      icons: [{
        icon: lineSymbol,
        offset: '0%'
      }],
      map: map
    });
    // start animation
    animateCircle(wholeDist);
  });

  $('#stop').click(() => {
    $('#startStop-modal').modal('show');
  });

  $('#actualStop').click(() => {
    $('#startStop-modal').modal('hide');
    $('#actual-stopping-modal').modal('show');
    $('#actualStoppingText').text('You have chosen to stop the ride prematurely. Please choose the spot to get off or let the car decide.');
    $('#actualStopAudio').get(0).play();
    $('.modal__locations-wrap').addClass('d-none');
      $('.modal__stopping-btn').addClass('d-none');
    setTimeout(() => {
      $('.modal__locations-wrap').removeClass('d-none');
      $('.modal__stopping-btn').removeClass('d-none');
    }, 6500);

    $('.modal__location-img, .modal__stopping-btn').click(() => {
      $('#actualStopAudioPart2').get(0).play();
      $('.modal__locations-wrap').addClass('d-none');
      $('.modal__stopping-btn').addClass('d-none');
      $('#actualStoppingText').text('The stopping process will take usually 30 seconds to a minute, depending on the car position and traffic.');
      setTimeout(() => {
        $('#actualStoppingText').text('The vehicle has stopped, you can now safely get off. Thank you for using our services.');
        $('#stoppedAudio').get(0).play();
      }, 30000);
    });



  });

  // settings buttons
  $('#s11').click(() => toggleSetting('#s1'));
  $('#s22').click(() => toggleSetting('#s2'));
  $('#s33').click(() => toggleSetting('#s3'));
  $('#s44').click(() => toggleSetting('#s4'));
  $('#s55').click(() => toggleSetting('#s5'));
  $('#s66').click(() => toggleSetting('#s6'));

  // helper to see what was clicked on
  // $(document).click(function (event) {
  //   console.log($(event.target));
  // });
});


/*
* functions
*/
// stoping modal
const stoppingModal = (almost = false) => {
  let text = almost ? 'Finding the suitable place to stop to let you off. Depending on the current traffic and car location, this may take some time (usually 30sec to a minute)' : 'We are nearing the destination. Please choose the spot to get off or let the car decide.';

  $('#stoppingTextClose').text(text);
  almost ? $('.modal__locations-wrap, .modal__stopping-btn').addClass('d-none') : $('#almostThereAudio').get(0).play();
  almost ? setTimeout(() => {
    $('#stoppingTextClose').text('The vehicle has stopped, you can now safely get off. Thank you for using our services.');
    $('#stoppedAudio').get(0).play();
  }, 4000) : '';
  $('#stopping-modal-close').modal('show');
};

// init aircon
const initAircon = () => {
  const knob = pureknob.createKnob(300, 300);
  knob.setProperty('angleStart', -0.70 * Math.PI);
  knob.setProperty('angleEnd', 0.70 * Math.PI);
  knob.setProperty('colorFG', '#fff');
  knob.setProperty('colorBG', 'lightgrey');
  knob.setProperty('colorLabel', 'rgba(#fff');
  knob.setProperty('trackWidth', 0.4);
  knob.setProperty('valMin', 10);
  knob.setProperty('valMax', 45);
  knob.setProperty('label', 'Climate control');
  knob.setValue(25);

  const node = knob.node();
  const elem = document.getElementById('aircon');
  elem.appendChild(node);
};

// initmap
const initMap = () => {
  $(window).on('load', () => {
    let startLocation = 'Eeden, Kalda tee 1c, 50703 Tartu';//{ lat: 58.372800, lng: 26.753930 };
    let endLocation = 'Rüütli 18, 51005 Tartu';//{ lat: 58.382020, lng: 26.721140 };
    $(startLocationContainer).text(startLocation);
    $(endLocationContainer).text(endLocation);
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: { lat: 58.378397, lng: 26.734660 },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: false,
      preserveViewport: true,
      map: map,
      panel: document.getElementById('right-panel'),
    });

    // route change
    directionsDisplay.addListener('directions_changed', function () {
      computeTotalDistance(directionsDisplay.getDirections());
    });

    displayRoute(startLocation, endLocation, directionsService,
      directionsDisplay);
  });
}

let id;
function animateCircle(wholeDistInM) {
  let count = 1;
  let offset;
  let sentiel = -1;
  let nrOfPoints = currentCoords.length;
  let smallDistanceInM = wholeDistInM / nrOfPoints;
  let intervalTimeInMs = parseInt((smallDistanceInM / 100) * 6000);
  console.log('nr of points: ' + nrOfPoints);
  console.log('small dist: ' + smallDistanceInM);
  console.log('whole dist: ' + wholeDistInM);
  console.log('interval: ' + intervalTimeInMs);
  console.log('speed: ' + settings.fun.speed);

  id = window.setInterval(function () {
    count = (count + 1) % 200;
    offset = count / 2;
    // we have only one icon
    let icons = line.get('icons');
    icons[0].offset = (offset) + '%';
    line.set('icons', icons);

    if (line.get('icons')[0].offset == '95%') {
      stoppingModal(false);
    }
    if (line.get('icons')[0].offset == "99.5%") {
      icons[0].offset = '100%';
      line.set('icons', icons);
      window.clearInterval(id);
    }
    if (line.get('icons')[0].offset == '100%') {
      stoppingModal(true);
    }
  }, intervalTimeInMs);
}

function displayRoute(origin, destination, service, display) {
  service.route({
    origin: origin,
    destination: destination,
    travelMode: 'DRIVING',
    provideRouteAlternatives: true,
  }, function (response, status) {
    if (status === 'OK') {
      // console.log(response.routes[0].overview_polyline);
      // console.log(response.routes[0].legs[0].steps);
      let polyline = require('@mapbox/polyline');
      coordsRoute1 = response.routes[0] ? polyline.decode(response.routes[0].overview_polyline) : [];
      coordsRoute2 = response.routes[1] ? polyline.decode(response.routes[1].overview_polyline) : [];
      coordsRoute3 = response.routes[2] ? polyline.decode(response.routes[2].overview_polyline) : [];

      for (var i = 0, len = response.routes.length; i < len; i++) {
        new google.maps.DirectionsRenderer({
          map: map,
          directions: response,
          routeIndex: i,
          draggable: false,
          preserveViewport: true,
        });
      }
      display.setDirections(response);
    } else {
      alert('Could not display directions due to: ' + status);
    }
  });
}

function computeTotalDistance(result) {
  var total = 0;
  var myroute;
  if (!started) {
    myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
      total += myroute.legs[i].distance.value;
    }
    total = total / 1000;
    total = Math.round((total + Number.EPSILON) * 100) / 100;
    document.getElementById('total').innerHTML = total + ' km';
  }
}
window.initMap = initMap;



// hide all
const hideAll = () => {
  $('.pages').children().hide();
};

// navigation
const hidePages = () => {
  $('.main-page, .car-page, .contact-page, .ent-page, .settings-page, .info-page').hide();
};

const changePage = (button, page, fadeSpeed) => {
  $(button).click(() => {
    hidePages();
    $('.navbar-collapse').removeClass('show');
    $('#nav-icon2').removeClass('open').addClass('collapsed');
    $('.nav-item').removeClass('active');
    $(button).parent().addClass('active');
    $(page).fadeIn(fadeSpeed);
  });
};

// settings buttons
const toggleSetting = (id) => {
  if (id == '#s1') {
    if ($(id).parents('.background').hasClass('background--on')) {
      $('.keepDistance').text('Off');
    } else {
      $('.keepDistance').text('On');
    }
  }
  $(id).parents('.background').toggleClass('background--on');
  $(id).parent('.toggle-body').toggleClass('toggle-body--on');
  $(id).toggleClass('toggle-btn--on');
  $(id).toggleClass('toggle-btn--scale');
};

const toggleDrivingStyle = (drivingStyleButton, drivingStyleSlider) => {
  drivingStyleButton.toggleClass('driving-style__button--right');
  drivingStyleSlider.toggleClass('driving-style--right');
  if (drivingStyleButton.hasClass('driving-style__button--right')) {
    $('.fa-caret-right').css('display', 'none');
    $('.fa-caret-left').css('display', 'inline-block');
    $('.drivingStyle').text(settings.exiting.text);
    $('#style-modal').modal('show');
    closeModalTimeout('style-modal');
    currentSpeedContainer.text(settings.exiting.speed);
    currentDistanceContaienr.text(settings.exiting.distance);
  }
  else {
    $('.fa-caret-right').css('display', 'inline-block');
    $('.fa-caret-left').css('display', 'none');
    $('.drivingStyle').text(settings.safest.text);
    $('#style-modal').modal('show');
    closeModalTimeout('style-modal');
    currentSpeedContainer.text(settings.safest.speed);
    currentDistanceContaienr.text(settings.safest.distance);
  }
}

const moodRangeSlider = () => {
  let range = $('.range');
  let inSafe = false;
  let inExiting = false;
  range.each(function () {
    let rangeThumb = $(this).next('.range-thumb');
    let max = parseInt(this.max, 10);
    let pixelsXaxis = parseInt(910, 10);

    $(this).on('input input.range', function () {
      let width = $(this).width(); // 1920px cant get width 'cause items are hidden
      let rangeValue = parseInt(this.value, 10);
      var text = rangeValue > max ? '∞' : rangeValue;
      pixelsXaxis = (rangeValue * (width - 100) / max);
      if (rangeValue >= 0 && rangeValue < 25) {
        text = settings.exiting.text + ' \u203A';
        $(range).alterClass('range--*', 'range--red');
        drivingStyleButton.addClass('driving-style__button--right');
        drivingStyleSlider.addClass('driving-style--right');
        $('.fa-caret-right').css('display', 'none');
        $('.fa-caret-left').css('display', 'inline-block');
        $('.drivingStyle').text(settings.exiting.text);
        if (!inExiting) {
          $('#style-modal').modal('show');
          closeModalTimeout('style-modal');
          inExiting = true;
        }
        currentDistanceContaienr.text(settings.exiting.distance);
        currentSpeedContainer.text(settings.exiting.speed);
        inSafe = false;
      }
      else if (rangeValue >= 25 && rangeValue < 50) {
        text = '\u2039 ' + settings.fun.text + ' \u203A';
        $(range).alterClass('range--*', 'range--yellow');
        currentDistanceContaienr.text(settings.fun.distance);
        currentSpeedContainer.text(settings.fun.speed);
        inExiting = false;
        inSafe = false;
      }
      else if (rangeValue >= 51 && rangeValue < 75) {
        text = '\u2039 ' + settings.safe.text + ' \u203A';
        $(range).alterClass('range--*', 'range--blue');
        currentDistanceContaienr.text(settings.safe.distance);
        currentSpeedContainer.text(settings.safe.speed);
        inExiting = false;
        inSafe = false;
      }
      else if (rangeValue >= 75 && rangeValue < 101) {
        text = '\u2039 ' + settings.safest.text;
        $(range).alterClass('range--*', 'range--green');
        drivingStyleButton.removeClass('driving-style__button--right');
        drivingStyleSlider.removeClass('driving-style--right');
        $('.fa-caret-right').css('display', 'inline-block');
        $('.fa-caret-left').css('display', 'none');
        $('.drivingStyle').text(settings.safest.text);
        if (!inSafe) {
          $('#style-modal').modal('show');
          closeModalTimeout('style-modal');
          inSafe = true;
        }
        currentDistanceContaienr.text(settings.safest.distance);
        currentSpeedContainer.text(settings.safest.speed);
        inExiting = false;
      }
      else {
        text = '\u2039 ' + settings.neutral.text + ' \u203A';
        $(range).alterClass('range--*', 'range--green');
        if (started) {
          currentDistanceContaienr.text(settings.neutral.distance);
          currentSpeedContainer.text(settings.neutral.speed);
        }
        else {
          currentDistanceContaienr.text(settings.neutral.distance);
          currentSpeedContainer.text('00');
        }

        inExiting = false;
        inSafe = false;
      }

      rangeThumb.css({ left: pixelsXaxis }).attr("data-val", text);
    });
  });
  range.trigger('input.range');
  $(window).on("resize", () => range.trigger("input.range"));
};

const closeModalTimeout = (modal) => {
  setTimeout(() => {
    $('#' + modal).modal('hide');
  }, 3000);
};
/**
 * jQuery alterClass plugin
 *
 * Remove element classes with wildcard matching. Optionally add classes:
 *   $( '#foo' ).alterClass( 'foo-* bar-*', 'foobar' )
 *
 * Copyright (c) 2011 Pete Boere (the-echoplex.net)
 * Free under terms of the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 */
(function ($) {
  $.fn.alterClass = function (removals, additions) {
    let self = this;
    if (removals.indexOf('*') === -1) {
      self.removeClass(removals);
      return !additions ? self : self.addClass(additions);
    }

    let patt = new RegExp('\\s' +
      removals.
        replace(/\*/g, '[A-Za-z0-9-_]+').
        split(' ').
        join('\\s|\\s') +
      '\\s', 'g');

    self.each(function (i, it) {
      let cn = ' ' + it.className + ' ';
      while (patt.test(cn)) {
        cn = cn.replace(patt, ' ');
      }
      it.className = $.trim(cn);
    });
    return !additions ? self : self.addClass(additions);
  };
})($);

// timer
let CountDown = (function ($) {
  // Length ms
  let TimeOut = 10000;
  // Interval ms
  let TimeGap = 1000;

  let CurrentTime = (new Date()).getTime();
  let EndTime = (new Date()).getTime() + TimeOut;

  let GuiTimer = $('.eta');
  let GuiPause = $('#pause');
  let GuiResume = $('#resume').hide();

  let Running = true;

  let UpdateTimer = function () {
    // Run till timeout
    if (CurrentTime + TimeGap < EndTime) {
      setTimeout(UpdateTimer, TimeGap);
    }
    // Countdown if running
    if (Running) {
      CurrentTime += TimeGap;
      if (CurrentTime >= EndTime) {
        GuiTimer.css('color', 'red');
      }
    }
    // Update Gui
    let Time = new Date();
    Time.setTime(EndTime - CurrentTime);
    let Minutes = Time.getMinutes();
    let Seconds = Time.getSeconds();

    GuiTimer.html(
      (Minutes < 10 ? '0' : '') + Minutes
      + ':'
      + (Seconds < 10 ? '0' : '') + Seconds);
  };

  let Pause = function () {
    Running = false;
    GuiPause.hide();
    GuiResume.show();
  };

  let Resume = function () {
    Running = true;
    GuiPause.show();
    GuiResume.hide();
  };

  let Start = function (Timeout) {
    TimeOut = Timeout;
    CurrentTime = (new Date()).getTime();
    EndTime = (new Date()).getTime() + TimeOut;
    UpdateTimer();
  };

  return {
    Pause: Pause,
    Resume: Resume,
    Start: Start
  };
})($);

const con = (input) => {
  console.log(input);
};
